swaylock \
  --grace 10 \ # If we touch the computer within 10 seconds of locking, no need for password
  --fade-in 0.1 \ # Fade in the lockscreen for 1/10th of a second
  --screenshots \ # Take a screenshot, and use it as the background image of lockscreen
  --effect-scale 0.5 \ # Scale the image by 1/2 (not sure)
  --effect-blur 7x3 \ # Blur the image
  --effect-scale 2 \ # Scale it back up
  --effect-vignette 0.5:0.5 \ # Vignette the image
  --indicator \ # Always show the indicator
  --indicator-radius 100 \ # How big the indicator should be
  --indicator-thickness 7 \ # Thickness of the radius of the typing notifier ring thing
  --clock \ # Display the clock in the indicator
  --ring-color 2a2e38 \
  --key-hl-color 51afef \
  --text-color bbc2cf \
  --line-color 2a2e38 \
  --inside-color 242730 \
  --separator-color 00000000 \
  timeout 60 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
  before-sleep 'swaylock -f -c 000000'
