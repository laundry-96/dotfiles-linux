local gears = require("gears")
local awful = require("wibox")
local wibox = require("wibox")
local dpi = require("beautiful.xresources").apply_dpi

local math, string, os = math, string, os
local my_table = gears.table

local theme = {}

theme.dir = os.getenv("HOME") .. "/.config/awesome/themes/laundry"
theme.wallpaper = os.getenv("HOME") .."/Pictures/wallpaper.png"
theme.font = "Noto Sans Mono 10"
theme.fg_normal                                 = "#FEFEFE"
theme.fg_focus                                  = "#32D6FF"
theme.fg_urgent                                 = "#C83F11"
theme.bg_normal                                 = "#222222"
theme.bg_focus                                  = "#1E2320"
theme.bg_urgent                                 = "#3F3F3F"
theme.taglist_fg_focus                          = "#00CCFF"
theme.tasklist_bg_focus                         = "#222222"
theme.tasklist_fg_focus                         = "#00CCFF"
theme.border_width = dpi(2)
theme.border_normal                             = "#3F3F3F"
theme.border_focus                              = "#6F6F6F"
theme.border_marked                             = "#CC9393"
theme.titlebar_bg_focus                         = "#3F3F3F"
theme.titlebar_bg_normal                        = "#3F3F3F"
theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = theme.fg_focus
theme.menu_height = dpi(16)
theme.menu_width = dpi(140)



return theme
