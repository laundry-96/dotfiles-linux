set nocompatible
filetype on
filetype plugin on
syntax on
set number
set shiftwidth=2
set tabstop=2
set expandtab
set incsearch
set ignorecase
set hlsearch
set showmatch

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif
